# Code for mRSA method

A set of R scripts for the application of Algorithm mRSA performing the maximization of Regional Sensitivity Indices in order to reveal sensitive model behaviors.

This repository is an add-on of:  Roux S.,  Loisel P. and Buis S. *Maximizing Regional Sensitivity Analysis indices to find sensitive model behaviors.*  Submitted to International Journal for Uncertainty Quantification. 2024

![incid](mrsa_cantis_x9.png)
# Installation

Please clone or download the repository and consider the R project mrsa_paper.Rproj.
mRSA Algorithm requires the R software (version >= 4.1.2) and the following R packages:
* sensitivity (>=1.28.1)
* parallel
* ggplot2
* latex2exp
* reshape2
# Content

The repository contains the following R source code files:

* [`mrsa_example_toy2D.Rmd`](https://forgemia.inra.fr/sebastien.roux/mrsa_paper/-/blob/main/mrsa_example_toy2D.Rmd) includes both the R code and its results for the application on a toy model with 2D ouputs.

* [`mrsa_example_cantis.Rmd`](https://forgemia.inra.fr/sebastien.roux/mrsa_paper/-/blob/main/mrsa_example_cantis.Rmd) includes both the R code and its results for the application on a the CANTIS environmental model. 

* [`mrsa_solve.R`](https://forgemia.inra.fr/sebastien.roux/mrsa_paper/-/blob/main/mrsa_solve.R) includes the R code for the optimization of region based sensitivity indices

* [`mrsa_plot.R`](https://forgemia.inra.fr/sebastien.roux/mrsa_paper/-/blob/main/mrsa_plot.R) includes the R code for the vizualisation of results for both the 2D and the Cantis models

and two Rdata files:
* `data_toy2D.Rdata` contains matrices X (parameters) and y (simulations) for the toy model with 2D ouputs.

* `data_cantis.Rdata` contains matrices X (parameters) and y (simulations) for the CANTIS model.

It also contains a RStudio project file (mrsa_paper.Rproj) that can be used by RStudio users.

# Usage
The repository contains two Rmarkdown files (*mrsa_example_toy2D.Rmd*, *mrsa_example_cantis.Rmd*) that can be easily run to reproduce results presented in the article. These files might also be adapted to your own model.

# Authors 

* Sébastien Roux MISTEA INRAE
* Patrice Loisel MISTEA INRAE
* Samuel Buis 	EMMAH 

# License
GPL-3 - @sebastien.roux INRAE MISTEA 2024


